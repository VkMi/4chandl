﻿using Generic_parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Threading;
using System.ComponentModel;

namespace _4chanImgDL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //global variables
        static string dl_folder_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\4chandl";
        private int pbmax, pbnow;
        private string html;
        static string path_to_first_file_dl = "";
        static bool isPTFFDmodifiable = true;

        public MainWindow()
        {
            InitializeComponent();
            if (!Directory.Exists(dl_folder_path)) Directory.CreateDirectory(dl_folder_path);
        }

        //event triggered by pressing play, mainly calls worker_dowork
        private void btn_download_Click(object sender, RoutedEventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            WebClient client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36)");
            client.Encoding = Encoding.UTF8;
            try
            {
                html = client.DownloadString(txtin_url.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Invalid url");
                throw;
            }
            if (html != "")
            {
                worker.RunWorkerAsync();
            }
        }

        //contains most of the logic
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //throw new NotImplementedException();
            List<string> all_files = Parser.get_multiple_from_1_whats_between_2_and_3(html, "File:", "</a>");
            string title = Parser.get_from_1_whats_between_2_and_3(html, "<meta name=\"description\" content=\"", " - &quot;/");
            if (Regex.IsMatch(title, @"[\/*?<>|:]"))
            {
                title = Regex.Split(title, @"[\/*?<>|:]")[0];
            }
            else if (title.Contains("\""))
            {
                title = Regex.Split(title, "\"")[0];
            }
            pbmax = all_files.Count;
            pbnow = 0;
            (sender as BackgroundWorker).ReportProgress(pbnow);
            foreach (string line in all_files)
            {
                dl_image(Parser.get_from_1_whats_between_2_and_3(line, "href=\"//", "\""),
                         Regex.Split(line, "target=\"_blank\">")[1],
                         title);
                pbnow++;
                (sender as BackgroundWorker).ReportProgress(pbnow);
            }
            //MessageBox.Show("Download completed!");
            if (path_to_first_file_dl != "")
                System.Diagnostics.Process.Start(path_to_first_file_dl);
            isPTFFDmodifiable = true;
        }

        //instruction called to update the progressbar
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            update_window();
        }

        //save image from "url" with "name" in "foldername"
        private static void dl_image(string url, string name, string foldername)
        {
            WebClient downloader = new WebClient();
            string dl_specific_folder = dl_folder_path + "\\" + foldername;
            if (!Directory.Exists(dl_specific_folder))
                Directory.CreateDirectory(dl_specific_folder);

            //remove illegal character from filename
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            foreach (char c in invalid)
            {
                name = name.Replace(c.ToString(), "");
            }

            if (!File.Exists(dl_specific_folder + "\\" + name))
            {
                if (isPTFFDmodifiable)
                {
                    path_to_first_file_dl = dl_specific_folder + "\\" + name;
                    isPTFFDmodifiable = false;
                }
                downloader.DownloadFile(new Uri("http://" + url), dl_specific_folder + "\\" + name);
            }
        }

        #region Support functions
        private void txtin_url_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            txtin_url.Text = Clipboard.GetText();
        }

        private void update_window()
        {
            prg_downloaded.Maximum = pbmax;
            prg_downloaded.Value = pbnow;
            txt_prog.Text = pbnow + "/" + pbmax;
        }

        #endregion
    }
}
